﻿using UnityEngine;
using System.Collections;

public class EnnemyManager : MonoBehaviour {

	public GameObject cochon;                // The enemy prefab to be spawned.
	public float spawnTime = 3f;            // How long between each spawn.
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	public GameObject rb;

	void Start ()
	{
		// Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}

	void Spawn ()
	{
		// Find a random index between zero and one less than the number of spawn points.
		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Quaternion rotation = spawnPoints[spawnPointIndex].rotation;
		rotation.x = rotation.x + Random.Range (0, 10);
		rotation.y = rotation.y + Random.Range (0, 10);

		// Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.

		rb = Instantiate (cochon, spawnPoints[spawnPointIndex].position, rotation) as GameObject;
		//rb.rigidbody.AddForce (new Vector3 (150,200,150));	
	}
}
